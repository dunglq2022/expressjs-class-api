class Companies {
    constructor(id, company, contact, country) {
        this.id = id;
        this.company = company;
        this.contact = contact;
        this.country = country;
    }
}

let companiesObject = [];
let company1 = new Companies(1, 'Alfreds Futterkiste', 'Maria Anders', 'Germany')
companiesObject.push(company1)

let company2 = new Companies(2, 'Centro comercial Moctezuma', 'Francisco Chang', 'Mexico')
companiesObject.push(company2)

let company3 = new Companies(3, 'Ernst Handel', 'Roland Mendel', 'Austria')
companiesObject.push(company3)

let company4 = new Companies(4, 'Island Trading', 'Helen Bennett', 'UK')
companiesObject.push(company4)

let company5 = new Companies(5, 'Laughing Bacchus Winecellars', 'Yoshi Tannamuri', 'Canada')
companiesObject.push(company5)

let company6 = new Companies(6, 'Magazzini Alimentari Riuniti', 'Giovanni Rovelli', 'Italy')
companiesObject.push(company6)

module.exports = {companiesObject}