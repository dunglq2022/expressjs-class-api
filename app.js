//Khai báo express
const express = require('express');

//Khai báo port
const port = 8000;

//Tạo app
const app = express();

app.use(express.json());

//Import router
const {companiesRouter} = require ('./app/router/companyRouter')

app.get('/', (req, res) => {
    res.json({
        message: `App running on port ${port}`,
    })
})

app.use('/', companiesRouter);

//Listen app
app.listen(port, () => {
    console.log('Server running at http://localhost:' + port);
})