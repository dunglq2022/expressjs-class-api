const getAllCompanies = (req, res, next) => {
    console.log('GET ALL COMPANIES')
    next();
}

const getOnlyCompany = (req, res, next) => {
    console.log('GET ONLY COMPANY')
    next();
}

const createCompany = (req, res, next) => {
    console.log('CREATE COMPANY')
    next();
}

const updateCompanyById = (req, res, next) => {
    console.log('UPDATE COMPANY BY ID')
    next();
}

const deleteCompanyById = (req, res, next) => {
    console.log('DELETE COMPANY BY ID')
    next();
}

module.exports = {
    getAllCompanies,
    getOnlyCompany,
    createCompany,
    updateCompanyById,
    deleteCompanyById
}