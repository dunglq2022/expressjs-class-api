//Khai báo express
const express = require ('express');

//Tạo router
const companiesRouter = express.Router();

//Import Dat
const {companiesObject} = require ('../../data')

//Import middileware
const {
    getAllCompanies,
    getOnlyCompany,
    createCompany,
    updateCompanyById,
    deleteCompanyById
} = require ('../middileware/companyMiddileware')

companiesRouter.get('/companies', getAllCompanies, (req, res) => {
    res.status(200).json({
        status: 'Get All Success',
        companies: companiesObject
    })
})

companiesRouter.get('/companies/:companyId', getOnlyCompany, (req, res) => {
    let companyId = req.params.companyId;
    for (let index = 0; index < companiesObject.length; index ++) {
        if (companiesObject[index].id.toString() === companyId) {
            res.status(200).json({
                status: 'Get Only Success',
                company: companiesObject[index]
            })
        }
    }
})

companiesRouter.post('/companies/', createCompany, (req, res) => {
    let bodyCompany = req.body;
    let id = companiesObject.length + 1;
    bodyCompany.id = id;
    companiesObject.push(bodyCompany);
    res.status(201).json({
        status: `Create Success`,
        company: bodyCompany
    })
})

companiesRouter.put('/companies/:companyId', updateCompanyById, (req, res) => {
    let companyId = req.params.companyId;
    let companyIndex = companiesObject.find(company => company.id.toString() === companyId)
    let bodyCompany = req.body
    if (companyIndex) {
        companyIndex.company = bodyCompany.company;
        companyIndex.contact = bodyCompany.contact;
        companyIndex.country = bodyCompany.country;
        res.status(200).json({
            message: `Update Success`,
            company: bodyCompany
        })
    }
})

companiesRouter.delete('/companies/:companyId', deleteCompanyById, (req, res) => {
    let companyId = req.params.companyId;
    let companyIndex = companiesObject.find(company => company.id.toString() === companyId);
    if (companyIndex) {
        companiesObject.splice(companyId -1, 1);
        res.status(200).json({
            message: `Delete Success`,
            company: companyIndex
        })
    }
})

module.exports = { companiesRouter };
